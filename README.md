[![pipeline status](https://gitlab.com/foodprojectdemo/webclient-error-handler-spring-boot-starter/badges/main/pipeline.svg)](https://gitlab.com/foodprojectdemo/webclient-error-handler-spring-boot-starter/-/commits/main)
[![coverage report](https://gitlab.com/foodprojectdemo/webclient-error-handler-spring-boot-starter/badges/main/coverage.svg)](https://gitlab.com/foodprojectdemo/webclient-error-handler-spring-boot-starter/-/commits/main)

# WebClient error handler spring boot starter

Provides retry and error handling