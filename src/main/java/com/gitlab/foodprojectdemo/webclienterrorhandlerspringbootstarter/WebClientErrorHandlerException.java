package com.gitlab.foodprojectdemo.webclienterrorhandlerspringbootstarter;

import lombok.Getter;
import lombok.NonNull;

@Getter
public class WebClientErrorHandlerException extends RuntimeException {
    private final int statusCode;
    private final String errorMessage;
    private final String errorCode;

    WebClientErrorHandlerException(@NonNull Throwable cause) {
        super("An internal error has occurred. Please try again later.", cause);
        this.statusCode = 0;
        this.errorMessage = "";
        this.errorCode = "";
    }

    WebClientErrorHandlerException(@NonNull String message, @NonNull Throwable cause) {
        super(message, cause);
        this.statusCode = 0;
        this.errorMessage = "";
        this.errorCode = "";
    }

    WebClientErrorHandlerException(@NonNull String message, @NonNull Throwable cause, int statusCode, String errorMessage, String errorCode) {
        super(message, cause);
        this.statusCode = statusCode;
        this.errorMessage = errorMessage;
        this.errorCode = errorCode;
    }
}
