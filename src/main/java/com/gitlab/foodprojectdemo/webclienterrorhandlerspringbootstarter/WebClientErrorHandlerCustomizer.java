package com.gitlab.foodprojectdemo.webclienterrorhandlerspringbootstarter;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.reactive.function.client.WebClientCustomizer;
import org.springframework.web.reactive.function.client.WebClient.Builder;

@Slf4j
@AllArgsConstructor
public class WebClientErrorHandlerCustomizer implements WebClientCustomizer {
    private final WebClientErrorHandler webClientErrorHandler;

    @Override
    public void customize(Builder webClientBuilder) {
        webClientBuilder.filter(webClientErrorHandler);
    }
}
