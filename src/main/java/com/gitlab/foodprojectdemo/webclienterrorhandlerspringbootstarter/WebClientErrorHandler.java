package com.gitlab.foodprojectdemo.webclienterrorhandlerspringbootstarter;

import com.jayway.jsonpath.JsonPath;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.reactive.function.client.ClientRequest;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.ExchangeFilterFunction;
import org.springframework.web.reactive.function.client.ExchangeFunction;
import org.springframework.web.reactive.function.client.WebClientRequestException;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.core.publisher.Mono;
import reactor.util.retry.Retry;
import reactor.util.retry.RetryBackoffSpec;

import java.time.Duration;
import java.util.Set;
import java.util.concurrent.TimeoutException;

@Slf4j
public class WebClientErrorHandler implements ExchangeFilterFunction {
    private static final Set<HttpStatus> SERVER_PROBLEMS = Set.of(
            HttpStatus.BAD_GATEWAY,
            HttpStatus.SERVICE_UNAVAILABLE,
            HttpStatus.GATEWAY_TIMEOUT
    );
    private final WebClientErrorHandlerProperties properties;

    public WebClientErrorHandler(WebClientErrorHandlerProperties properties) {
        this.properties = properties;
        log.info("Using properties: {}", properties);
    }

    @Override
    public @NonNull Mono<ClientResponse> filter(@NonNull ClientRequest request, ExchangeFunction next) {
        return next.exchange(request)
                .timeout(Duration.ofMillis(properties.getTimeoutInMilliseconds()))
                .doOnError(throwable -> log.info(throwable.toString()))
                .retryWhen(retryBackoffSpec())
                .flatMap(this::createExceptionIfErrorResponse)
                .onErrorResume(WebClientResponseException.class, this::toResponseException)
                .onErrorMap(this::errorMapper)
                .doOnError(throwable -> log.info("Error handling result: {}", throwable.toString()));
    }

    private RetryBackoffSpec retryBackoffSpec() {
        return Retry.backoff(properties.getMaxAttempts(), Duration.ofMillis(properties.getMinBackoffInMilliseconds()))
                .filter(this::isServerProblemOrTimeout)
                .maxBackoff(Duration.ofMillis(properties.getMaxBackoffInMilliseconds()))
                .doAfterRetry(retrySignal -> log.info("Retry " + retrySignal.totalRetries()))
                .onRetryExhaustedThrow((retryBackoffSpec, retrySignal) -> new TimeoutException());
    }

    private Mono<ClientResponse> createExceptionIfErrorResponse(ClientResponse clientResponse) {
        if (clientResponse.statusCode().isError()) {
            return clientResponse.createException().flatMap(Mono::error);
        }
        return Mono.just(clientResponse);
    }

    private Mono<ClientResponse> toResponseException(WebClientResponseException exception) {
        return Mono.fromCallable(() -> {
            log.info("Received response: {}", exception.getResponseBodyAsString());
            var context = JsonPath.parse(exception.getResponseBodyAsString());
            var error = context.read(properties.getErrorPath(), String.class);
            var message = context.read(properties.getMessagePath(), String.class);
            return new ErrorResponse(error, message);
        }).onErrorReturn(new ErrorResponse("", ""))
                .doOnNext(errorResponse -> log.info("ErrorResponse: {}", errorResponse))
                .flatMap(errorResponse -> Mono.<ClientResponse>error(ResponseException.create(exception, errorResponse)))
                .doOnError(throwable -> log.info("Created ResponseException: {}", throwable.toString()));
    }

    private Throwable errorMapper(Throwable throwable) {
        log.info("Caught an exception: {}", throwable.toString());
        if (throwable instanceof ResponseException) {
            return throwable;
        } else if (throwable instanceof WebClientRequestException || throwable instanceof TimeoutException) {
            return new ServiceIsNotAvailableException(throwable);
        } else {
            return new WebClientErrorHandlerException(throwable);
        }
    }

    private boolean isServerProblemOrTimeout(Throwable throwable) {
        return throwable instanceof TimeoutException ||
                (throwable instanceof WebClientResponseException && isServerProblem((WebClientResponseException) throwable));
    }

    private boolean isServerProblem(WebClientResponseException responseException) {
        return SERVER_PROBLEMS.contains(responseException.getStatusCode());
    }

}
