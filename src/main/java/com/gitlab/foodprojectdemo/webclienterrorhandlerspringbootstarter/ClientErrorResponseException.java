package com.gitlab.foodprojectdemo.webclienterrorhandlerspringbootstarter;

import org.springframework.web.reactive.function.client.WebClientResponseException;

public class ClientErrorResponseException extends ResponseException {

    ClientErrorResponseException(WebClientResponseException responseException, ErrorResponse errorResponse) {
        super("An internal error has occurred. Please try again later.", responseException, errorResponse);
    }

    ClientErrorResponseException(String message, WebClientResponseException responseException, ErrorResponse errorResponse) {
        super(message, responseException, errorResponse);
    }
}
