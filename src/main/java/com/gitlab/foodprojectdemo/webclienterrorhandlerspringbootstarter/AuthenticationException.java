package com.gitlab.foodprojectdemo.webclienterrorhandlerspringbootstarter;

import org.springframework.web.reactive.function.client.WebClientResponseException;

public class AuthenticationException extends ClientErrorResponseException {

    AuthenticationException(WebClientResponseException responseException, ErrorResponse errorResponse) {
        super(errorResponse.getMessage().isEmpty() ? "Authentication Error" : errorResponse.getMessage(), responseException, errorResponse);
    }
}
