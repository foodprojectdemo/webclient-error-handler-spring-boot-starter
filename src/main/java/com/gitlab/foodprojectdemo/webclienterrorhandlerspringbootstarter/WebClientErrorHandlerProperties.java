package com.gitlab.foodprojectdemo.webclienterrorhandlerspringbootstarter;

import lombok.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;

@Value
@ConstructorBinding
@ConfigurationProperties(prefix = "webclient-error-handler")
public class WebClientErrorHandlerProperties {
    Integer timeoutInMilliseconds;
    Integer minBackoffInMilliseconds;
    Integer maxBackoffInMilliseconds;
    Integer maxAttempts;
    String errorPath;
    String messagePath;

    public Integer getTimeoutInMilliseconds() {
        return getOrDefault(timeoutInMilliseconds, 3000);
    }

    public Integer getMinBackoffInMilliseconds() {
        return getOrDefault(minBackoffInMilliseconds, 1000);
    }

    public Integer getMaxBackoffInMilliseconds() {
        return getOrDefault(maxBackoffInMilliseconds, 3000);
    }

    public Integer getMaxAttempts() {
        return getOrDefault(maxAttempts, 5);
    }

    public String getErrorPath() {
        return getOrDefault(errorPath, "$.error");
    }

    public String getMessagePath() {
        return getOrDefault(messagePath, "$.message");
    }

    private <V> V getOrDefault(V value, V defaultValue) {
        return (value != null) ? value : defaultValue;
    }

}

