package com.gitlab.foodprojectdemo.webclienterrorhandlerspringbootstarter;

import org.springframework.web.reactive.function.client.WebClientResponseException;

public class ServerErrorResponseException extends ResponseException {

    ServerErrorResponseException(WebClientResponseException responseException, ErrorResponse errorResponse) {
        super("The service is not available. Please try again later.", responseException, errorResponse);
    }
}
