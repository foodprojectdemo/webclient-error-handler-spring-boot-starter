package com.gitlab.foodprojectdemo.webclienterrorhandlerspringbootstarter;

public class ServiceIsNotAvailableException extends WebClientErrorHandlerException {

    ServiceIsNotAvailableException(Throwable throwable) {
        super("The service is not available. Please try again later.", throwable);
    }

}
