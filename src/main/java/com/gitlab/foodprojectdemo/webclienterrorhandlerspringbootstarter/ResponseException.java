package com.gitlab.foodprojectdemo.webclienterrorhandlerspringbootstarter;

import org.springframework.web.reactive.function.client.WebClientResponseException;

public class ResponseException extends WebClientErrorHandlerException {

    static ResponseException create(WebClientResponseException responseException, ErrorResponse errorResponse) {
        if (responseException.getStatusCode().is4xxClientError()) {
            switch (responseException.getRawStatusCode()) {
                case 401:
                    return new AuthenticationException(responseException, errorResponse);
                case 403:
                    return new ForbiddenException(responseException, errorResponse);
                case 400:
                case 404:
                default:
                    return new ClientErrorResponseException(responseException, errorResponse);
            }
        } else if (responseException.getStatusCode().is5xxServerError()) {
            return new ServerErrorResponseException(responseException, errorResponse);
        } else {
            return new ResponseException("An internal error has occurred. Please try again later.", responseException, errorResponse);
        }
    }

    ResponseException(String message, WebClientResponseException responseException, ErrorResponse errorResponse) {
        super(message, responseException, responseException.getRawStatusCode(), errorResponse.getMessage(), errorResponse.getError());
    }
}
