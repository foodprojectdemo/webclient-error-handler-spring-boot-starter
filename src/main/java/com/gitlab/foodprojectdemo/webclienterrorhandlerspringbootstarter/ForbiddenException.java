package com.gitlab.foodprojectdemo.webclienterrorhandlerspringbootstarter;

import org.springframework.web.reactive.function.client.WebClientResponseException;

public class ForbiddenException extends ClientErrorResponseException {

    ForbiddenException(WebClientResponseException responseException, ErrorResponse errorResponse) {
        super(errorResponse.getMessage().isEmpty() ? "Illegal action" : errorResponse.getMessage(), responseException, errorResponse);
    }
}
