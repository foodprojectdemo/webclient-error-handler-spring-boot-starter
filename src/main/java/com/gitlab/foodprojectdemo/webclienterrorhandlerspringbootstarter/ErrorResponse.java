package com.gitlab.foodprojectdemo.webclienterrorhandlerspringbootstarter;

import lombok.Value;

@Value
class ErrorResponse {
    String error;
    String message;
}
