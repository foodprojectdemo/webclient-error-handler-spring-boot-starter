package com.gitlab.foodprojectdemo.webclienterrorhandlerspringbootstarter;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration(proxyBeanMethods = false)
@EnableConfigurationProperties(WebClientErrorHandlerProperties.class)
public class WebClientErrorHandlerAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public WebClientErrorHandlerCustomizer webClientErrorHandler(WebClientErrorHandlerProperties properties) {
        return new WebClientErrorHandlerCustomizer(new WebClientErrorHandler(properties));
    }

}
