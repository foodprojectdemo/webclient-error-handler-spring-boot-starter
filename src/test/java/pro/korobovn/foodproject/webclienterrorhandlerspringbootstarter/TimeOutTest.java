package com.gitlab.foodprojectdemo.webclienterrorhandlerspringbootstarter;

import com.github.tomakehurst.wiremock.WireMockServer;
import org.apache.http.HttpHeaders;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class TimeOutTest extends AbstractTest {

    @Test
    void shouldReturnServiceIsNotAvailableException(WebClient webClient, WireMockServer mockServer) {
        mockServer.stubFor(get("/").willReturn(aResponse()
                .withStatus(HttpStatus.OK.value())
                .withFixedDelay(10000)
                .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE))
        );

        assertThatThrownBy(() -> webClient.get().uri("/").retrieve().bodyToMono(Void.class).block())
                .hasFieldOrPropertyWithValue("errorCode","")
                .isInstanceOf(ServiceIsNotAvailableException.class);
    }

}