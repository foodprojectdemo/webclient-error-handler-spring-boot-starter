package com.gitlab.foodprojectdemo.webclienterrorhandlerspringbootstarter;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ExtensionContext.Namespace;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.ParameterResolver;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.reactive.function.client.WebClient;

@Slf4j
public class WireMockExtension implements BeforeAllCallback, AfterAllCallback, ParameterResolver {


    @Override
    public void beforeAll(ExtensionContext context) {
        var options = WireMockConfiguration.options().dynamicPort();
        var wireMockServer = new WireMockServer(options);
        wireMockServer.start();
        setWireMockServer(wireMockServer, context);
    }

    @Override
    public void afterAll(ExtensionContext context) {
        getWireMockServer(context).shutdown();
    }

    @Override
    public boolean supportsParameter(ParameterContext parameterContext, ExtensionContext extensionContext) throws ParameterResolutionException {
        return parameterContext.getParameter().getType().equals(WireMockServer.class) ||
                parameterContext.getParameter().getType().equals(WebClient.Builder.class) ||
                parameterContext.getParameter().getType().equals(WebClient.class);
    }

    @Override
    public Object resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext) throws ParameterResolutionException {
        if (parameterContext.getParameter().getType().equals(WireMockServer.class)) {
            return getWireMockServer(extensionContext);
        } else if (parameterContext.getParameter().getType().equals(WebClient.Builder.class)) {
            return getWebClientBuilder(extensionContext);
        } else if (parameterContext.getParameter().getType().equals(WebClient.class)) {
            return getWebClientBuilder(extensionContext).build();
        }
        return null;
    }

    private WebClient.Builder getWebClientBuilder(ExtensionContext context) {
        var applicationContext = SpringExtension.getApplicationContext(context);
        return applicationContext.getBean(WebClient.Builder.class)
                .baseUrl(String.format("http://localhost:%d/", getWireMockServer(context).port()));
    }

    private void setWireMockServer(WireMockServer wireMockServer, ExtensionContext context) {
        context.getStore(Namespace.create(WireMockExtension.class)).put(WireMockServer.class, wireMockServer);
    }

    private WireMockServer getWireMockServer(ExtensionContext context) {
        return (WireMockServer) context.getStore(Namespace.create(WireMockExtension.class)).get(WireMockServer.class);
    }
}
