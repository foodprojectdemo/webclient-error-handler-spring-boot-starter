package com.gitlab.foodprojectdemo.webclienterrorhandlerspringbootstarter;

import com.github.tomakehurst.wiremock.WireMockServer;
import lombok.SneakyThrows;
import org.apache.http.HttpHeaders;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.util.SocketUtils;
import org.springframework.web.reactive.function.client.WebClient;

import java.nio.file.Files;
import java.util.Objects;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;

class ErrorMappingTest extends AbstractTest {

    private static final String ERROR_CODE = "ERROR_CODE";
    private static final String AN_ERROR_HAS_OCCURRED = "An error has occurred";

    @Value("classpath:payload/response.json")
    private Resource response;

    @Test
    void shouldReturnServiceIsNotAvailableException(WebClient.Builder builder) {
        var webClient = builder.baseUrl(String.format("http://localhost:%d/", SocketUtils.findAvailableTcpPort())).build();

        try {
            webClient.get().uri("/").retrieve().bodyToMono(Void.class).block();
            fail("Should have occurred an error");
        } catch (WebClientErrorHandlerException e) {
            assertThat(e).isInstanceOf(ServiceIsNotAvailableException.class);
            assertThat(e.getMessage()).isEqualTo("The service is not available. Please try again later.");
        }
    }

    @Test
    void shouldReturnAuthenticationException(WebClient webClient, WireMockServer mockServer) {
        testTemplate(
                webClient,
                mockServer,
                HttpStatus.UNAUTHORIZED,
                AuthenticationException.class,
                AN_ERROR_HAS_OCCURRED
        );
    }

    @Test
    void shouldReturnForbiddenException(WebClient webClient, WireMockServer mockServer) {
        testTemplate(
                webClient,
                mockServer,
                HttpStatus.FORBIDDEN,
                ForbiddenException.class,
                AN_ERROR_HAS_OCCURRED
        );
    }

    public static Stream<Arguments> serverErrorStatuses() {
        return httpStatusProvider(500, 599);
    }

    @ParameterizedTest
    @MethodSource("serverErrorStatuses")
    void shouldReturnServerErrorResponseException(HttpStatus httpStatus, WebClient webClient, WireMockServer mockServer) {
        testTemplate(
                webClient,
                mockServer,
                httpStatus,
                ServerErrorResponseException.class,
                "The service is not available. Please try again later."
        );
    }

    public static Stream<Arguments> clientErrorStatuses() {
        return httpStatusProvider(400, 499);
    }

    @ParameterizedTest
    @MethodSource("clientErrorStatuses")
    void shouldReturnClientErrorResponseException(HttpStatus httpStatus, WebClient webClient, WireMockServer mockServer) {
        testTemplate(
                webClient,
                mockServer,
                httpStatus,
                ClientErrorResponseException.class,
                "An internal error has occurred. Please try again later."
        );
    }

    private void testTemplate(
            WebClient webClient,
            WireMockServer mockServer,
            HttpStatus httpStatus,
            Class<? extends WebClientErrorHandlerException> expectedException,
            String expectedMessage
    ) {
        configureMock(mockServer, httpStatus);

        try {
            webClient.get().uri("/").retrieve().bodyToMono(Void.class).block();
            fail("Should have occurred an error");
        } catch (WebClientErrorHandlerException e) {
            assertThat(e).isInstanceOf(expectedException);
            assertThat(e.getMessage()).isEqualTo(expectedMessage);
            assertThat(e.getErrorMessage()).isEqualTo(AN_ERROR_HAS_OCCURRED);
            assertThat(e.getErrorCode()).isEqualTo(ERROR_CODE);
            assertThat(e.getStatusCode()).isEqualTo(httpStatus.value());
        }
    }


    private void configureMock(WireMockServer mockServer, HttpStatus httpStatus) {
        mockServer.stubFor(get("/").willReturn(aResponse()
                .withStatus(httpStatus.value())
                .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .withBody(getResponse()))
        );
    }


    private static Stream<Arguments> httpStatusProvider(int min, int max) {
        return IntStream.range(min, max)
                .mapToObj(value -> {
                    try {
                        return HttpStatus.valueOf(value);
                    } catch (IllegalArgumentException e) {
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .filter(value -> value != HttpStatus.UNAUTHORIZED && value != HttpStatus.FORBIDDEN)
                .map(Arguments::of);
    }

    @SneakyThrows
    private String getResponse() {
        return Files.readString(response.getFile().toPath());
    }
}