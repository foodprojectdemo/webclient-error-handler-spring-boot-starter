package com.gitlab.foodprojectdemo.webclienterrorhandlerspringbootstarter;

import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.autoconfigure.web.reactive.function.client.WebClientAutoConfiguration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith({SpringExtension.class, WireMockExtension.class})
@ContextConfiguration(classes = {JacksonAutoConfiguration.class, WebClientAutoConfiguration.class, WebClientErrorHandlerAutoConfiguration.class})
public abstract class AbstractTest {
}
